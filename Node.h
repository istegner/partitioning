#pragma once
#include <vector>
#include "Tile.h"
#include "AABB.h"
#include "TiledWorldGenerator.h"

class Node
{
public:
	AABBf bounds; //Bounding Box(AABB)
	Node* parent; //parent node
	std::vector<Node*> children; //Children nodes
	std::vector<Tile*> contents; //Tile contents
	int treeLevel;
	int minSizeToSplit = 2;
	int minObjectToSpit = 5;

	Node();
	Node(Node* _parent);
	~Node();

	void AddObject(Tile* _tile);
	std::vector<Tile*> FindRelevantTiles(Vector2f target);
	bool CheckSplit();
};

