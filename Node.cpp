#include "Node.h"

AABBf bounds; //Bounding Box(AABB)
Node* parent = nullptr; //parent node
std::vector<Node*> children; //Children nodes
std::vector<Tile*> contents; //Tile contents
int layer;

Node::Node()
{
	parent = nullptr;
}

Node::Node(Node* _parent)
{
	parent = _parent;
}

Node::~Node()
{

}

void Node::AddObject(Tile* _tile)
{
	//If we already have children
	if (children.size() > 0)
	{
		//Loop over children
		for (auto child : children)
		{
			//Add object if overlap
			if (child->bounds.Intersects(_tile->bounds))
			{
				child->AddObject(_tile);
			}
		}
	}
	//No children
	else
	{
		//Add object to node
		contents.push_back(_tile);
		//Check if needs to split
		if (CheckSplit())
		{
			//Split
			auto min = Vector2f::Zero;
			auto max = Vector2f::Zero;
			//Create the children
			auto childTopLeft = new Node(this);
			min = Vector2f(bounds.boxMin.X, bounds.Centre().Y);
			max = Vector2f(bounds.Centre().X, bounds.boxMax.Y);
			childTopLeft->bounds = AABBf(min, max);
			childTopLeft->treeLevel = treeLevel + 1;
			children.push_back(childTopLeft);

			auto childTopRight = new Node(this);
			min = bounds.Centre();
			max = bounds.boxMax;
			childTopRight->bounds = AABBf(min, max);
			childTopRight->treeLevel = treeLevel + 1;
			children.push_back(childTopRight);

			auto childBottomLeft = new Node(this);
			min = bounds.boxMin;
			max = bounds.Centre();
			childBottomLeft->bounds = AABBf(min, max);
			childBottomLeft->treeLevel = treeLevel + 1;
			children.push_back(childBottomLeft);

			auto childBottomRight = new Node(this);
			min = Vector2f(bounds.Centre().X, bounds.boxMin.Y);
			max = Vector2f(bounds.boxMax.X, bounds.Centre().Y);
			childBottomRight->bounds = AABBf(min, max);
			childBottomRight->treeLevel = treeLevel + 1;
			children.push_back(childBottomRight);

			//Loop over children
			for (auto child : children)
			{
				//Loop over objects
				for (auto _tile_ : contents)
				{
					//Add object if overlap
					if (child->bounds.Intersects(_tile->bounds))
					{
						child->AddObject(_tile);
					}
				}
			}
		}
	}
}

std::vector<Tile*> Node::FindRelevantTiles(Vector2f target)
{
	if (children.size() > 0)
	{
		for (auto child : children)
		{
			if (child->bounds.Contains(target))
			{
				return child->FindRelevantTiles(target);
			}
		}
	}
	return contents;
}

bool Node::CheckSplit()
{
	if (contents.empty())
		return false;
	else if (contents.size() >= minObjectToSpit && (bounds.Width() >= minSizeToSplit && bounds.Height() >= minSizeToSplit))
		return true;
	return false;
}
